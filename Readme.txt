Projekt "Tranzycje obrazu oparte o efekty przenikania si� obraz�w z wykorzystaniem frameworka OpenCL"

====================================================================================

Programista:Micha� Krakiewicz

Sk�ad zespo�u:
Tomasz Bernacki
Marcelina Jasku�a
Grzegorz Kad�uczka 
Micha� Krakiewicz
Dominik Nogajczyk


Grupa 331 IZZ

====================================================================================

Release notes:

v0.6.0:
    -Implemented "Slide in" corners functionality (left|right)(up|down)
    -Amended code structure to allow compile as Dynamic Library
    -Compiling resources into *.dll
    
v0.5.0
    -Implemented compiling *.cl files into *.exe as resource - one of two methods is
 chosen at compile time (Visual Studio resource(*.rc) or *.o files using objcopy)
     
v0.4.0
    -Compiles under Linux - currently Linux is for testing only, Shared library not
 tested / supported
    -Bug fixes and amendments
    
v0.3.0
    -Using FILE pointers instead of streams - faster reading and writing
    -Bug fixes
    
v0.2.0
    -Major code cleanup & refactor
    
v0.1.0
    -Initial "sandbox" with working OpenCL kernel and partial Crossfade functionality
    