@ECHO off
setlocal enableDelayedExpansion
ECHO Skrypt prezentuje dzialanie aplikacji przetwarzajacej efekty na projekt 2014 - grupa 331 IZZ.
ECHO.

set Polecenie="Projekt grupowy - Konsola.exe"
set KatalogWyjsciowy=test_data
set Plik1=test_data\a.yuv
set Plik2=test_data\b.yuv
set Wysokosc=1080
set Szerokosc=1920
set WybranyEfekt=0
set TylkoWypisz=0
set NumerKarty=0


ECHO Opcje:
ECHO.
ECHO 1. Wygenerowanie plikow wideo wszystkich efektow z wszystkimi kombinacjami kierunkow (wolne)
ECHO 2. Wygenerowanie plikow wideo dla jednego efektu z wszystkimi kombinacjami kierunkow
ECHO 3. Wygenerowanie po jednym pliku wideo dla kazdego efektu
ECHO 4. Wygenerowanie testowego pliku dla jednego efektu
ECHO.


set /p NumerKarty=Prosze wybrac numer karty: 

:lWyborCyfry
set Wybor=0
set /p Wybor=Prosze podac cyfre od 1 do 4: 

rem echo %Wybor%| findstr /r "^[1-9][0-9]*$"

if %Wybor% LSS 1 goto lWyborCyfry
if %Wybor% GTR 4 goto lWyborCyfry
ECHO Wybrano %Wybor%

if %Wybor% EQU 1 goto lPominWyborEfektu
if %Wybor% EQU 3 goto lPominWyborEfektu


ECHO Prosze wybrac efekt:
ECHO.
ECHO 1. Efekt Crossfade - plynne przejscie.
ECHO 2. Efekt Slide in - efekt przesuwania obrazow.

:lWyborEfektu
set /p WyborEfektu=Prosze podac cyfre 1-2: 
if %WyborEfektu% LSS 1 goto lWyborEfektu
if %WyborEfektu% GTR 2 goto lWyborEfektu

set WybranyEfekt=%WyborEfektu%

:lPominWyborEfektu


set StaleParametry=-w %Szerokosc% -h %Wysokosc% -f1 "%Plik1%" -f2 "%Plik2%" -l 150

if %WybranyEfekt% EQU 0 (
  call:crossfade
  call:slidein
) else (
  if %WybranyEfekt% EQU 1 call :crossfade
  if %WybranyEfekt% EQU 2 call :slidein

)

pause
goto:eof


rem ----------------------------------------------------------

:crossfade
ECHO Generowanie efektu Crossfade.
set Efekt=crossfade
set P=%Polecenie% -e %Efekt% %StaleParametry% -of %KatalogWyjsciowy%/%Efekt%.yuv
if %TylkoWypisz% EQU 1 (
  echo !P!
) ELSE (
  !P!
)
goto:eof

:slidein
ECHO Generowanie efektu Slide-in.
set Efekt=slidein
set N=0;
set kierunki[0]=up
if (%Wybor%) LSS (3) (
  set kierunki[1]=down
  set kierunki[2]=left
  set kierunki[3]=right
  set kierunki[4]=rightup
  set kierunki[5]=rightdown
  set kierunki[6]=leftup
  set kierunki[7]=leftdown
  set N=7;
)
for /L %%a in (0,1,%N%) do (
  set Kierunek=!kierunki[%%a]!
  set P=%Polecenie% -e %Efekt% %StaleParametry% -of %KatalogWyjsciowy%/%Efekt%_!Kierunek!.yuv -d !Kierunek!
  if %TylkoWypisz% EQU 1 (
    echo !P!
  )  ELSE (
    !P!
  )
)
goto:eof