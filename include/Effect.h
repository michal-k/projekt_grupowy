/*
 * Effect.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef EFFECT_H_
#define EFFECT_H_

#include <string>
#include "structs.h"

class BufferedYUVFrameReader;
class BufferedYUVFrameWriter;
class OpenCLAdapter;
class ProcessorParameters;
class FrameCalculator;

class Effect {
public:
   const std::string EffectName;

   const YUVFileParams params;
   const size_t StartFrame;


protected:

   BufferedYUVFrameReader *inputFile1, *inputFile2;
   
   BufferedYUVFrameWriter *outputFile;

   size_t InputFrameCount;

   FrameCalculator  *fcalc;

   Effect();
public:

   const size_t EffectLength;

   Effect(const char *EffectName, const ProcessorParameters &tok);

   int getOutputFrames( const ProcessorParameters &tok );


   bool process();

   virtual std::string getProgramSource() = 0;

   virtual ~Effect();

protected:
   virtual void initGlobalWorksize(OpenCLAdapter& mgr);

private:
   void printWriteFrameMessage();
   void copyFramesUntilEnd();
   int getEffectLength(const ProcessorParameters& tok);
   void copyFramesNoInterpolation();
   bool isInterpolation();
   size_t getInputFrames();

  const ProcessorParameters &parameters;
};

#endif /* Effect_H_ */
