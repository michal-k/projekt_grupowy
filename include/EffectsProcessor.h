/*
 * helpers.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef EFFECTSPROCESSOR_H_
#define EFFECTSPROCESSOR_H_

#include <string>
#include "dll_export.h"

class ProcessorParameters;

class EffectsProcessor
{
   size_t lastOpenCLStatus;
   std::string lastError;
   ProcessorParameters *parameters;
public:
   PROCESSOR_API EffectsProcessor();

   PROCESSOR_API bool setFromConsoleArguments(const int argc, const char **argv);
   PROCESSOR_API bool setValue(const char *Id, const char *value);

   PROCESSOR_API bool process();
   PROCESSOR_API std::string getLastError() const;

   PROCESSOR_API virtual ~EffectsProcessor();

};



#endif /* OPENCLHELPERS_H_ */
