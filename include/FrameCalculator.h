/*
* FrameCalculator.h
*
*  Created on: May 7, 2014
*      Author: cr3
*/

#ifndef FRAMECALCULATOR_H_
#define FRAMECALCULATOR_H_

class FrameCalculator {
public:
    const unsigned int InputFrames;
    const unsigned int OutputFrames;
    const double FrameScaleFactor;
private:
    double currentInputFrame;
    unsigned int currentOutputFrame;

    void setOutputFrames(unsigned int outputFrames);
    double getFrameFactor(unsigned int outputFrames, unsigned int inputFrames);

public:
    FrameCalculator(unsigned int inputFrames, unsigned int outputFrames);

    void nextFrame();
    double getCurrentInputFrame() const;
    unsigned int getCurrentOutputFrame() const;
    bool isSameFrameCount();

    virtual ~FrameCalculator();
};

#endif /* FRAMECALCULATOR_H_ */
