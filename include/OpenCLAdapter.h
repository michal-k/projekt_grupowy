/*
 * Effect.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef OpenCLAdapter_H_
#define OpenCLAdapter_H_

#include "CL/cl.h"

class OpenCLAdapter {
    int deviceNumber;
	size_t datasize;

    cl_int status;
    cl_platform_id *platforms;
    cl_uint numDevices;
    cl_device_id *devices;
    cl_context context;


	cl_mem clBufferA;  // Input array on the device
	cl_mem clBufferB;  // Input array on the device
    cl_mem clBufferA_previous;  // Input array on the device
    cl_mem clBufferB_previous;
	cl_mem clBufferC; // Output
	cl_mem clBufferFrameNo;

	cl_command_queue cmdQueue;
    cl_program program;
    cl_kernel kernel;

    size_t *globalWorkSize;
    size_t workSizeArraySize;

  
public:

    OpenCLAdapter(size_t datasize,int cardNumber = -1);

	void buildProgram(const char* programSource);
	void createKernel(const char* functionName);

    bool process();

	virtual ~OpenCLAdapter();
	void setWorkSize(size_t *globalWorkSize,size_t arraySize);
    void setDeviceNumber(int val);   


	void writeInputBuffer1(char* buffer1,char* buffer1_previousFrame);
	void writeInputBuffer2(char* buffer2,char* buffer2_previousFrame);
	void writeOutputBuffer(char* outputBuffer);
	void setFrameNo(float effectFrameNo);
	void runKernel();
	void setOutputBuffer();

private:
	void cleanUp();
	cl_platform_id* initPlatforms();
	cl_device_id* initDevices();
	cl_context createContext();
	cl_command_queue createCmdQueue();
	void createBuffers();
};


#endif /* OpenCLAdapter_H_ */
