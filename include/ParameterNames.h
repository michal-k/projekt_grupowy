/*
 * ProcessorParameters.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef PARAMETERNAMES_H_
#define PARAMETERNAMES_H_

enum Parameters {
   EffectType,
   Width,
   Height,
   Star
};

#endif /* ProcessorParameters_H_ */
