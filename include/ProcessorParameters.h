/*
 * ProcessorParameters.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef PROCESSORPARAMETERS_H_
#define PROCESSORPARAMETERS_H_

#include <map>
#include <string>

class ProcessorParameters
{
   typedef std::map<std::string,std::string> StringMap;
   StringMap values;
public:
	ProcessorParameters();

	void setValue(const char *Id, const char *value);
	std::string getValue(const char *requstedId, bool optional = false) const;
	int getInt(const char *requstedId, bool optional = false) const;
    bool isValue(const char *requstedId) const;

	virtual ~ProcessorParameters();
};


#endif /* ProcessorParameters_H_ */
