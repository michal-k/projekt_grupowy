/*
 * helpers.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef SOURCEPROCESSOR_H_
#define SOURCEPROCESSOR_H_

#include <string>
struct YUVFileParams;

class SourceProcessor
{
    std::string source;
public:
SourceProcessor();

bool addSourceFromVisualStudioResource(int ID);

bool addSourceFromExternObj(char &start, char &end);
bool isSourceLoaded();
void replaceStrings(std::string toReplace, std::string value);
void replaceParams(const YUVFileParams &params);


std::string getSource();

virtual ~SourceProcessor();

private:

    std::string getSourceString( int ID );

};

#endif /* OPENCLHELPERS_H_ */
