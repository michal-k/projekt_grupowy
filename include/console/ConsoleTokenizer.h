/*
 * ProcessorParameters.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef CONSOLETOKENIZER_H_
#define CONSOLETOKENIZER_H_

class ProcessorParameters;

class ConsoleTokenizer
{
   const int &argc;
   const char **argv;
public:
	ConsoleTokenizer(const int &argc, const char **argv);

	void setParameters(ProcessorParameters*p);

	virtual ~ConsoleTokenizer();

};

#endif /* ProcessorParameters_H_ */
