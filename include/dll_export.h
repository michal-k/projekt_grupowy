/*
 * DLL_EXPORT_H_
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef DLL_EXPORT_H_
#define DLL_EXPORT_H_

#define DLL_NAME "Projekt grupowy.dll"
#ifdef DLL_EXPORTS
#define PROCESSOR_API __declspec(dllexport)
#else
#define PROCESSOR_API
#endif

#endif /* DLL_EXPORT_H_ */
