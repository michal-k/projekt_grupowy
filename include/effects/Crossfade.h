/*
 * Crossfade.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef CROSSFADE_H_
#define CROSSFADE_H_

#include "Effect.h"

class Crossfade : public Effect{

	Crossfade();
public:

	Crossfade(const ProcessorParameters &tok);

    virtual std::string getProgramSource();

	virtual ~Crossfade();
protected:
	virtual void initGlobalWorksize(OpenCLAdapter& mgr);
};

#endif /* CROSSFADE_H_ */
