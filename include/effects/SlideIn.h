/*
 * Crossfade.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef SLIDEIN_H_
#define SLIDEIN_H_

#include "Effect.h"


enum SlideDirections
{
   UP = 1,
   DOWN,
   LEFT,
   RIGHT,
   LEFTUP,
   RIGHTUP,
   LEFTDOWN,
   RIGHTDOWN,
};

class SourceProcessor;

class SlideIn: public Effect {
   SlideDirections direction;

   SlideIn();
   void replaceSourceTags(SourceProcessor& processor);

public:

   SlideIn(const ProcessorParameters &tok);

   virtual std::string getProgramSource();

   virtual ~SlideIn();


protected:
   SlideDirections setDirection(const ProcessorParameters &tok);
   virtual void initGlobalWorksize(OpenCLAdapter& mgr);
};

#endif /* SlideIn */
