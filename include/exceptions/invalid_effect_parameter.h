/*
 * InvalidEffectParameter.h
 *
 *  Created on: May 6, 2014
 *      Author: cr3
 */

#ifndef INVALIDEFFECTPARAMETER_H_
#define INVALIDEFFECTPARAMETER_H_

#include <stdexcept>
#include <string>
#ifndef _GLIBCXX_USE_NOEXCEPT
#define _GLIBCXX_USE_NOEXCEPT
#endif
class invalid_effect_parameter : public std::runtime_error{
public:
   invalid_effect_parameter(const std::string& paramName, const std::string& invalidValue);
   virtual ~invalid_effect_parameter()_GLIBCXX_USE_NOEXCEPT;

   const std::string& getInvalidValue() const
   {
      return invalidValue;
   }

   const std::string& getParamName() const
   {
      return paramName;
   }

private:
   const std::string& paramName;
   const std::string& invalidValue;
   const std::string combineMessage(const std::string& paramName,
         const std::string& invalidValue);
};

#endif /* INVALIDEFFECTPARAMETER_H_ */
