/*
 * mandatoryvaluenotprovided.h
 *
 *  Created on: May 7, 2014
 *      Author: cr3
 */

#ifndef MANDATORYVALUENOTPROVIDED_H_
#define MANDATORYVALUENOTPROVIDED_H_

#include <stdexcept>
#ifndef _GLIBCXX_USE_NOEXCEPT
#define _GLIBCXX_USE_NOEXCEPT
#endif
class mandatory_value_not_provided : public std::runtime_error {
public:
   mandatory_value_not_provided(const std::string& paramName);
   virtual ~mandatory_value_not_provided() _GLIBCXX_USE_NOEXCEPT;
};

#endif /* MANDATORYVALUENOTPROVIDED_H_ */
