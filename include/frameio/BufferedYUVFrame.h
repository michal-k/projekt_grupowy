/*
 * BufferedYUVFrame.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef BUFFEREDYUVFRAME_H_
#define BUFFEREDYUVFRAME_H_

class BufferedYUVFileProcessor {
public:
   const std::string Filename;
   const YUVFileParams params;
   const size_t FrameSize;
   const size_t BufferSize;
protected:
   char * buffer;
   virtual size_t getLength() = 0;
public:

   BufferedYUVFileProcessor(const std::string& Filename, const YUVFileParams &params);

   char * getBuffer() const;
   virtual void closeFile() = 0;

   virtual ~BufferedYUVFileProcessor();

};

#endif /* BUFFEREDYUVFRAME_H_ */
