/*
 * BufferedYUVFrameReader.h
 *
 *  Created on: Apr 2, 2014
 *      Author: Michal Krakiewicz
 */

#ifndef BUFFEREDYUVFRAMEREADER_H_
#define BUFFEREDYUVFRAMEREADER_H_

#include "BufferedYUVFrame.h"

class BufferedYUVFrameReader : public BufferedYUVFileProcessor
{
protected:
	  FILE *inputFile;
public:
  	const size_t FileLength;
    const size_t FrameCount;
protected:

      size_t remainingBytes;
      size_t framesRead;
      
public:

    BufferedYUVFrameReader(const std::string& Filename,const YUVFileParams &params);
    char * loadNextFrame();


    bool hasFrame();
    size_t getFramesRead() const;
    char * getPreviousFrameBuffer() const;

    virtual void closeFile();
   
    virtual ~BufferedYUVFrameReader();

protected:
    virtual size_t getLength();
private:
    char * previousFrameBuffer;
    
    void swapBuffers();
};


#endif /* BUFFEREDYUVFRAMEREADER_H_ */
