/*
 * BufferedYUVFrameWriter.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef BUFFEREDYUVFRAMEWRITER_H_
#define BUFFEREDYUVFRAMEWRITER_H_

#include "BufferedYUVFrame.h"

class BufferedYUVFrameWriter : public BufferedYUVFileProcessor
{
protected:
	FILE * outputFile;

public:

	BufferedYUVFrameWriter(const std::string& Filename,const YUVFileParams &params);
    void writeFrame();
    char* getBuffer();

    virtual ~BufferedYUVFrameWriter();

protected:
    void closeFile();
    virtual size_t getLength();
};


#endif /* BUFFEREDYUVFRAMEWRITER_H_ */
