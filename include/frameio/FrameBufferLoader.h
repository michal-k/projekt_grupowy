/*
 * Effect.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef FRAMEBUFFERLOADER_H_
#define FRAMEBUFFERLOADER_H_


class BufferedYUVFrameReader;
class FrameCalculator;

class FrameBufferLoader {

   BufferedYUVFrameReader *inputFile;
   FrameCalculator  *fcalc;
   size_t offset;

public:

   FrameBufferLoader(BufferedYUVFrameReader *inputFile,FrameCalculator  *fcalc, size_t offset = 0);
   char *getNextFrame();
   char *getPreviousFrame();

   virtual ~FrameBufferLoader();


};

#endif /* Effect_H_ */
