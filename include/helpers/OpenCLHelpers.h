/*
 * helpers.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef OPENCLHELPERS_H_
#define OPENCLHELPERS_H_

#include "CL/cl.h"

const char * get_error_string(cl_int err);
void d(cl_int status);
void listDevices();

#endif /* OPENCLHELPERS_H_ */
