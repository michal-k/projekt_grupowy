/*
 * helpers.h
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#ifndef HELPERS_H_
#define HELPERS_H_

#include <string>
#include <sstream>

bool is_number(const std::string& s);
template <class T> std::string to_string(const T &val)
{
    std::stringstream s;
    s << val;
    std::string t = s.str();
    return t;
}


#endif /* HELPERS_H_ */
