/*
 * SimpleLog.h
 *
 *  Created on: Apr 11, 2014
 *      Author: cr3
 */

#ifndef SIMPLELOG_H_
#define SIMPLELOG_H_

#include <string>
#include <sstream>

namespace LogLevels {

	enum LevelType {
//		Quiet,
		Info,
		Warning,
		Error,
//		All,
		LEVEL_COUNT

	};
}

class SimpleLog {
	bool *enabledLevels;
	std::string **levelNames;
	LogLevels::LevelType currentStreamLogLevel;
public:
	SimpleLog();
	void enableLevel(LogLevels::LevelType level);
	void disableLevel(LogLevels::LevelType level);

	void enableAll();
	void disableAll();

	void setLevel(LogLevels::LevelType level);
	bool isLevelEnabled(LogLevels::LevelType level);

	void log(LogLevels::LevelType level, const char *message);

	void setStreamLevel(LogLevels::LevelType level);
	void operator<< (const char *message)
	{
		_log(currentStreamLogLevel,message);
	}


//	void log(LogLevels::LevelType level, std::ostringstream& ss);

	virtual ~SimpleLog();

private:
	bool _log(LogLevels::LevelType &level, const char *message);
	std::string _getMsg(LogLevels::LevelType &level, const char *message);
	void resetTo(bool val);
	void initNames();
};





#endif /* SIMPLELOG_H_ */
