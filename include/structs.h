/*
* STRUCTS_H_
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/

#ifndef STRUCTS_H_
#define STRUCTS_H_

//#include <CL/cl.h>

struct YUVFileParams{
    const size_t Width, Height;
    const size_t YSize;
    const size_t USize;
    const size_t VSize;
    const size_t UBlockOffset;
    const size_t VBlockOffset;

    YUVFileParams(size_t Width, size_t Height):
    Width(Width),
        Height(Height),
        YSize(Height*Width),
        USize(YSize/4),
        VSize(YSize/4),
        UBlockOffset(YSize),
        VBlockOffset(UBlockOffset + USize)
    {
    }

};


#endif /* STRUCTS_H_ */
