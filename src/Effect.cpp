/*
* Effect.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "Effect.h"

#include "ProcessorParameters.h"

#include <fstream>
// System includes
#include <stdio.h>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <cmath>
// OpenCL includes
#include <CL/cl.h>
#include "frameio/BufferedYUVFrameWriter.h"
#include "frameio/BufferedYUVFrameReader.h"
#include "frameio/FrameBufferLoader.h"

#include "OpenCLAdapter.h"
#include "FrameCalculator.h"

#include "loggers/SimpleLog.h"



Effect::Effect(const char *EffectName, const ProcessorParameters &tok) :
    EffectName(EffectName),
    params(YUVFileParams(tok.getInt("w"),tok.getInt("h"))), 
    StartFrame(tok.getInt("start", true)),
    
    inputFile1(new BufferedYUVFrameReader(tok.getValue("f1"), params)),
    inputFile2(new BufferedYUVFrameReader(tok.getValue("f2"), params)),
    outputFile(new BufferedYUVFrameWriter(tok.getValue("of"), params)),
    EffectLength(getEffectLength(tok)),
    fcalc(0),
    parameters(tok)
{
        InputFrameCount = getInputFrames();

        fcalc = new FrameCalculator(InputFrameCount, getOutputFrames(tok));
}

int Effect::getEffectLength(const ProcessorParameters& tok) {

    int result;
    try {
        result = tok.getInt("l", true);
    } catch (...) {

    }
    if (result == 0)
        result = inputFile1->FrameCount;
    return result;
}

void Effect::printWriteFrameMessage() {
    std::cout << "\rWriting frame " << fcalc->getCurrentOutputFrame() << " ("
        << outputFile->FrameSize / 1024 << " kB) " << std::flush;
}


bool Effect::process() {

    SimpleLog logger;
    logger.enableAll();
    /*logger.log(LogLevels::Info, "Beginning processing.");
    logger.log(LogLevels::Info, "Beginning processing.");
    logger << "Test Stream";*/
    // Compute the size of the data
    int cardNo = -1;
    if ( (parameters.isValue("card")))
    {
        cardNo = (parameters.getInt("card"));
    }
    OpenCLAdapter effectCL(inputFile1->BufferSize * sizeof(char),cardNo);


      

    //OpenCLAdapter interpolationCL(inputFile1->BufferSize * sizeof(char));

    std::cout << "Buffer size: " << inputFile1->BufferSize << " B, "
        << inputFile1->BufferSize / 1024 << " kB " << std::endl;
    //        std::cout <<"Size_t: " << sizeof(size_t)  << std::endl;


    char * buffer1 = 0;
    char * buffer1_previousFrame = 0;
    char * buffer2 = 0;
    char * buffer2_previousFrame = 0;

    FrameBufferLoader loader1(inputFile1, fcalc);
    FrameBufferLoader loader2(inputFile2, fcalc, StartFrame);
    initGlobalWorksize(effectCL);

    std::string source = getProgramSource();
    effectCL.buildProgram(source.c_str());
    effectCL.createKernel(EffectName.c_str());
    //	frameNo = 0;
    cl_ulong effectFrameNo = 0;


    buffer2=loader2.getNextFrame();
    buffer2_previousFrame = loader2.getPreviousFrame();

    buffer1 = loader1.getNextFrame();
    buffer1_previousFrame = loader1.getPreviousFrame();

    while (fcalc->getCurrentOutputFrame() < fcalc->OutputFrames) {


     //   if (fcalc->getCurrentOutputFrame() <  (StartFrame+EffectLength) )
     //   {
            buffer1 = loader1.getNextFrame();
            buffer1_previousFrame = loader1.getPreviousFrame();

           
    //    }
          // Use clEnqueueWriteBuffer() to write input array A to
        // the device buffer clBufferA
        if (fcalc->getCurrentOutputFrame() >= (StartFrame+1) )
        {
            buffer2=loader2.getNextFrame();
            if (buffer2 == 0)
                break;
            buffer2_previousFrame = loader2.getPreviousFrame();

          
        } 
          
        if (buffer1 && buffer1_previousFrame)
            effectCL.writeInputBuffer1(buffer1,buffer1_previousFrame);

        if (buffer2 &&buffer2_previousFrame )
            effectCL.writeInputBuffer2(buffer2,buffer2_previousFrame);


        effectCL.setFrameNo(fcalc->getCurrentInputFrame());

        effectCL.setOutputBuffer();

        effectCL.runKernel();
        //-----------------------------------------------------
        // STEP 12: Read the output buffer back to the host
        //-----------------------------------------------------
        //     clFinish(cmdQueue);

        char * buffer3 = outputFile->getBuffer();
        effectCL.writeOutputBuffer(buffer3);
        //-----------------------------------------------------
        // STEP 13: Release OpenCL resources
        //-----------------------------------------------------

        if (outputFile) {
            printWriteFrameMessage();
            outputFile->writeFrame();
        } else {
            std::cout << "Error! no input file " << std::endl;
            break;
        }
        effectFrameNo++;
        fcalc->nextFrame();
    }

    //    outputFile->closeFile();

    std::cout << "\rCompleted." << std::flush << std::endl;

    return true;
}

void Effect::initGlobalWorksize(OpenCLAdapter& mgr) {
    if (params.USize != params.VSize)
        throw std::logic_error(
        "Logic error: U block size must be the same as V!");
    int size = 1;
    size_t *tmpglobalWorkSize = new size_t[size];
    tmpglobalWorkSize[0] = params.USize;

    mgr.setWorkSize(tmpglobalWorkSize, size);
}

Effect::~Effect() {
    //	closeFiles();

    if (inputFile1)
        delete inputFile1;
    if (inputFile2)
        delete inputFile2;
    if (outputFile)
        delete outputFile;
    if (fcalc)
        delete fcalc;
}

size_t Effect::getInputFrames()
{
    return (inputFile1->FrameCount - StartFrame) + (inputFile2->FrameCount -EffectLength);
}

int Effect::getOutputFrames( const ProcessorParameters &tok )
{
    int result =  tok.getInt("oframes", true);
    if (result == 0)
        return InputFrameCount;

    return result;
}

