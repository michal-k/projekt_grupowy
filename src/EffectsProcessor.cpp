/*
 * ProcessorParameters.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "EffectsProcessor.h"
#include "ProcessorParameters.h"
#include "console/ConsoleTokenizer.h"
#include "effects/Crossfade.h"
#include "effects/SlideIn.h"

#include "helpers/OpenCLHelpers.h"

#include "dll_export.h"
#include "exceptions/mandatory_value_not_provided.h"
#include "exceptions/invalid_effect_parameter.h"

PROCESSOR_API EffectsProcessor::EffectsProcessor() :
      lastOpenCLStatus(0), parameters(new ProcessorParameters()) {

}

PROCESSOR_API bool EffectsProcessor::setFromConsoleArguments(const int argc,
      const char **argv) {

   bool processResult = true;

   try {
     ConsoleTokenizer tokenizer(argc,argv);
     tokenizer.setParameters(parameters);

   } catch (std::exception &e) {
      processResult = false;
      lastError = e.what();
   } catch (...) {
      processResult = false;
      lastError = "Unknown";
   }

   return processResult;
}

PROCESSOR_API std::string EffectsProcessor::getLastError() const {
   return lastError;
}

PROCESSOR_API bool EffectsProcessor::setValue(const char* Id,
      const char* value) {
   bool processResult = true;

   try {
      parameters->setValue(Id, value);

   } catch (std::exception &e) {
      processResult = false;
      lastError = e.what();
   } catch (...) {
      processResult = false;
      lastError = "Unknown";
   }
   return processResult;
}

PROCESSOR_API bool EffectsProcessor::process() {
   bool processResult = true;

   try {
      bool isList = parameters->isValue("listDevices");
      if (isList){
          listDevices();
          return true;
      }

      const std::string effect = parameters->getValue("e");
      if (effect == "crossfade") {
         Crossfade p(*parameters);
         p.process();
      } else if (effect == "slidein") {
         SlideIn p(*parameters);
         p.process();
      } else
         throw std::invalid_argument("Wrong effect type!");  
   } catch (std::exception &e) {
      processResult = false;
      lastError = e.what();
   } catch (...) {
      processResult = false;
      lastError = "Unknown";
   }

   //std::cin.get();

   return processResult;
}

PROCESSOR_API EffectsProcessor::~EffectsProcessor() {
   delete parameters;
}
