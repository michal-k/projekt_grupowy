/*
* FrameCalculator.cpp
*
*  Created on: May 7, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "FrameCalculator.h"

bool FrameCalculator::isSameFrameCount() {
    if (InputFrames == OutputFrames)
        return true;
    return false;
}

double FrameCalculator::getFrameFactor(unsigned int outputFrames,   unsigned int inputFrames)
{

    if (inputFrames == 0)
        throw std::invalid_argument("inputFrames count is 0!");

    return (double) (inputFrames) / (double) (outputFrames);
}

FrameCalculator::FrameCalculator(unsigned int inputFrames, unsigned int outputFrames) :
InputFrames(inputFrames), OutputFrames((outputFrames > 0) ? outputFrames : inputFrames), FrameScaleFactor(
    getFrameFactor(OutputFrames, InputFrames)),currentInputFrame(0), currentOutputFrame(0) {

}

void FrameCalculator::nextFrame() {
    currentOutputFrame++;
    currentInputFrame = currentOutputFrame*this->FrameScaleFactor;
}

double FrameCalculator::getCurrentInputFrame() const {
    return currentInputFrame;
}

FrameCalculator::~FrameCalculator() {
}

unsigned int FrameCalculator::getCurrentOutputFrame() const
{
    return currentOutputFrame;
}

