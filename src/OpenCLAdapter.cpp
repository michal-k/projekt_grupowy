/*
* OpenCLAdapter.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "helpers/OpenCLHelpers.h"

// System includes
#include <stdio.h>
#include <sstream>
#include <string.h>
#include <cmath>
// OpenCL includes
#include <CL/cl.h>

#include "loggers/SimpleLog.h"
#include "OpenCLAdapter.h"

#include <stdexcept>

OpenCLAdapter::OpenCLAdapter(size_t datasize, int cardNumber) :
deviceNumber(cardNumber),
datasize(datasize),
status(0),
platforms(initPlatforms()),
numDevices(0),
devices(initDevices()),
context(createContext()),
cmdQueue(createCmdQueue()),
program(0),
kernel(0),
globalWorkSize(0),
workSizeArraySize(0)


{
    createBuffers();

}



OpenCLAdapter::~OpenCLAdapter() {
    // Free OpenCL resources
    cleanUp();
}


void OpenCLAdapter::createBuffers() {
    //-----------------------------------------------------
    // STEP 5: Create device buffers
    //-----------------------------------------------------
    // Use clCreateBuffer() to create a buffer object (d_A)
    // that will contain the data from the host array A
    clBufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL,
        &status);

    clBufferA_previous = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL,
        &status);
    // Use clCreateBuffer() to create a buffer object (d_B)
    // that will contain the data from the host array B
    clBufferB = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL,
        &status);
    clBufferB_previous = clCreateBuffer(context, CL_MEM_READ_ONLY, datasize, NULL,
        &status);
    // Use clCreateBuffer() to create a buffer object (d_C)
    // with enough space to hold the output data
    clBufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, datasize, NULL,
        &status);
    clBufferFrameNo = clCreateBuffer(context, CL_MEM_READ_ONLY,
        sizeof(float), NULL, &status);
}

void OpenCLAdapter::buildProgram(const char* programSource) {
    // Create a program using clCreateProgramWithSource()
    program = clCreateProgramWithSource(context, 1,
        (const char**) &programSource, NULL, &status);
    // Build (compile) the program for the devices with
    // clBuildProgram()
    status = clBuildProgram(program, numDevices, devices,
        NULL/*"-cl-nv-verbose"*/, NULL, NULL);

    if (status == CL_BUILD_PROGRAM_FAILURE) {
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

        // Allocate memory for the log
        char *log = (char *) malloc(log_size);

        // Get the log
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

        // Print the log
        printf("%s\n", log);
    }
    if (status != 0)
        throw std::runtime_error(std::string("Build failed!: ") + get_error_string(status));
}

void OpenCLAdapter::createKernel(const char* functionName) {
    //-----------------------------------------------------
    // STEP 8: Create the kernel
    //-----------------------------------------------------
    kernel = NULL;
    // Use clCreateKernel() to create a kernel
    kernel = clCreateKernel(program, functionName, &status);
    d(status);
}

void OpenCLAdapter::setWorkSize(size_t *globalWorkSize, size_t arraySize) {
    // There are 'length' work-items
    this->globalWorkSize = globalWorkSize;
    this->workSizeArraySize = arraySize;
}

void OpenCLAdapter::setDeviceNumber(int val) {
    deviceNumber = val; 
}

void OpenCLAdapter::writeInputBuffer1(char* buffer1,char* buffer1_previousFrame) {
    status = clEnqueueWriteBuffer(cmdQueue, clBufferA, CL_FALSE, 0,
        datasize, buffer1, 0, NULL, NULL);
    d(status);
    status = clEnqueueWriteBuffer(cmdQueue, clBufferA_previous, CL_FALSE, 0,
        datasize, buffer1_previousFrame, 0, NULL, NULL);
    d(status);

    status |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &clBufferA);

    d(status);
    status |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &clBufferA_previous);

    d(status);
}

void OpenCLAdapter::writeInputBuffer2(char* buffer2,char* buffer2_previousFrame) {
    status = clEnqueueWriteBuffer(cmdQueue, clBufferB, CL_FALSE, 0,
        datasize, buffer2, 0, NULL, NULL);
    status |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &clBufferB);
    d(status);
    status = clEnqueueWriteBuffer(cmdQueue, clBufferB_previous, CL_FALSE, 0,
        datasize, buffer2_previousFrame, 0, NULL, NULL);
    status |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &clBufferB_previous);
    d(status);
}

void OpenCLAdapter::setOutputBuffer() {
    status |= clSetKernelArg(kernel, 5, sizeof(cl_mem), &clBufferC);
}

void OpenCLAdapter::writeOutputBuffer(char* outputBuffer) {
    status = clEnqueueReadBuffer(cmdQueue, clBufferC, CL_TRUE, 0,
        datasize, outputBuffer, 0, NULL, NULL);

    d(status);
}

void OpenCLAdapter::setFrameNo(float effectFrameNo) {

    status = clEnqueueWriteBuffer(cmdQueue, clBufferFrameNo, CL_FALSE, 0,
        sizeof(float), &effectFrameNo, 0, NULL, NULL);

    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &clBufferFrameNo);
    d(status);
}

void OpenCLAdapter::runKernel() {
    if (!globalWorkSize)
        throw std::logic_error("Global worksize not set");

    status = clEnqueueNDRangeKernel(cmdQueue, kernel, workSizeArraySize, NULL, globalWorkSize,
        NULL, 0, NULL, NULL);
    d(status);
}

void OpenCLAdapter::cleanUp() {
    // Free OpenCL resources
    if (globalWorkSize)
        delete [] globalWorkSize;

    if (kernel)
        clReleaseKernel(kernel);
    if (program)
        clReleaseProgram(program);

    if (cmdQueue)
        clReleaseCommandQueue (cmdQueue);
    if (clBufferA)
        clReleaseMemObject(clBufferA);
    if (clBufferA_previous)
        clReleaseMemObject(clBufferA_previous);
    if (clBufferB)
        clReleaseMemObject(clBufferB);
    if (clBufferB_previous)
        clReleaseMemObject(clBufferB_previous);
    if (clBufferC)
        clReleaseMemObject(clBufferC);
    if (context)
        clReleaseContext (context);
    if (platforms)
        free (platforms);
    if (devices)
        free (devices);
}

cl_platform_id* OpenCLAdapter::initPlatforms() {
    //-----------------------------------------------------
    // STEP 1: Discover and initialize the platforms
    //-----------------------------------------------------
    cl_uint numPlatforms = 0;
    cl_platform_id *platforms = NULL;
    // Use clGetPlatformIDs() to retrieve the number of
    // platforms
    status = clGetPlatformIDs(0, NULL, &numPlatforms);
    d(status);
    // Allocate enough space for each platform
    platforms =
        (cl_platform_id*) (malloc(numPlatforms * sizeof(cl_platform_id)));
    // Fill in platforms with clGetPlatformIDs()
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    d(status);
    return platforms;
}

cl_device_id* OpenCLAdapter::initDevices() {

    cl_device_id *devices = NULL;
    // Use clGetDeviceIDs() to retrieve the number of
    // devices present
    status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL,
        &numDevices);
    d(status);
    
    // Allocate enough space for each device
    devices = (cl_device_id*) (malloc(numDevices * sizeof(cl_device_id)));
    // Fill in devices with clGetDeviceIDs()
    status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, numDevices,
        devices, NULL);
    d(status);
    return devices;
}

cl_context OpenCLAdapter::createContext() {
    cl_context context = NULL;
    // Create a context using clCreateContext() and
    // associate it with the devices
    context = clCreateContext(NULL, numDevices, devices, NULL, NULL, &status);
    return context;
}

cl_command_queue OpenCLAdapter::createCmdQueue() {
    //-----------------------------------------------------
    // STEP 4: Create a command queue
    //-----------------------------------------------------
    cl_command_queue cmdQueue = 0;
    // Create a command queue using clCreateCommandQueue(),
    // and associate it with the device you want to execute
    // on

    if ((numDevices > 1)&&(deviceNumber == (-1)))
    {      
        listDevices();
        throw std::runtime_error("NumDevices > 1 but device not chosen. Please use -card parameter to specify card number");
    }

    int useNum = 0;
    if (deviceNumber >= 0)
        useNum = deviceNumber;

    cmdQueue = clCreateCommandQueue(context, devices[useNum], 0, &status);
    return cmdQueue;
}


