/*
 * ProcessorParameters.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "ProcessorParameters.h"
#include "exceptions/invalid_effect_parameter.h"
#include "exceptions/mandatory_value_not_provided.h"
#include "helpers/helpers.h"
#include <stdio.h>
#include <string>

ProcessorParameters::ProcessorParameters() {

}

ProcessorParameters::~ProcessorParameters() {
}

std::string ProcessorParameters::getValue(const char *requstedId,
      bool optional) const {

   StringMap::const_iterator iter = values.find(requstedId);
   std::string result = "";
   if (iter != values.end()) {
      result = (*iter).second;
   }
   if (result == "") {
      if (!optional) {

          throw mandatory_value_not_provided(requstedId);
      }
   }

   return result;
}

bool ProcessorParameters::isValue(const char *requstedId) const {

          StringMap::const_iterator iter = values.find(requstedId);

          if (iter != values.end()) {
              return true;
          }
          return false;
}

int ProcessorParameters::getInt(const char *requstedId,
      bool optional) const {
   std::string val;
   try {
      val = getValue(requstedId);
   } catch (...) {
      if (optional)
         return 0;
      throw;
   }

   if (is_number(val)) {
      int n;
      sscanf(val.c_str(), "%d", &n);
      return n;
   } else {
      throw invalid_effect_parameter(requstedId, val);
   }

}

void ProcessorParameters::setValue(const char* Id,
      const char* value) {
   values[Id] = value;
}
