///*
// * OpenCLEffect.cpp
// *
// *  Created on: Apr 2, 2014
// *      Author: cr3
// */
#include "stdafx.h"
#include "SourceProcessor.h"
#include <string>
#include <iostream>
#include "structs.h"
#include "helpers/helpers.h"
#ifdef VISUALSTUDIO
#include "Windows.h"
#include "resource.h"
#include "dll_export.h"
#endif

SourceProcessor::SourceProcessor() {
    source = "";
}

SourceProcessor::~SourceProcessor() {

}

#ifdef VISUALSTUDIO
bool SourceProcessor::addSourceFromVisualStudioResource( int ID )
{
    source = source + getSourceString(ID);

    // std::cout << s << std::endl;

    if (source.length() == 0)
        return false;
    return true;
}
#endif

bool SourceProcessor::addSourceFromExternObj(char &start, char &end) {

    source = source + std::string(&start, &end);
    std::cout << source << std::endl;
    if (source.length() == 0)
        return false;
    return true;
}

void SourceProcessor::replaceParams(const YUVFileParams &params) {
    replaceStrings("{Width}", to_string<size_t>(params.Width));
    replaceStrings("{Height}", to_string<size_t>(params.Height));

    replaceStrings("{UBlockOffset}", to_string<size_t>(params.UBlockOffset));
    replaceStrings("{VBlockOffset}", to_string<size_t>(params.VBlockOffset));

    replaceStrings("{USize}", to_string<size_t>(params.USize));
    replaceStrings("{YSize}", to_string<size_t>(params.YSize));
    replaceStrings("{VSize}", to_string<size_t>(params.VSize));
}

void SourceProcessor::replaceStrings(std::string toReplace, std::string value) {
    while (source.find(toReplace) != std::string::npos)
        source.replace(source.find(toReplace), toReplace.length(), value.c_str());

}

bool SourceProcessor::isSourceLoaded() {
    if (source.length() == 0)
        return false;
    return true;
}

std::string SourceProcessor::getSource() {
 //   std::cout << source << std::endl;
    return source;
}

std::string SourceProcessor::getSourceString( int ID )
{
    std::string source = "";

    HRSRC hRes = NULL;
    HMODULE hMod = GetModuleHandle(DLL_NAME);
    hRes = FindResource( hMod, MAKEINTRESOURCE(ID) , MAKEINTRESOURCE(KernelFile) );
    if (!hRes)
        throw std::runtime_error("Couldn't load kernel source from DLL!");
    HGLOBAL hData = LoadResource( hMod, hRes );
    LPVOID data = LockResource( hData );
    source = (LPCTSTR )data; // magic here
    return source;
}

