/*
 * ConsoleTokenizer.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "console/ConsoleTokenizer.h"
#include "ProcessorParameters.h"
#include "helpers/helpers.h"
#include <stdexcept>
#include <stdio.h>
#include <string.h>

ConsoleTokenizer::ConsoleTokenizer(const int &argc, const char **argv) :
argc(argc),argv(argv)
{

}

ConsoleTokenizer::~ConsoleTokenizer() {
}

void ConsoleTokenizer::setParameters(ProcessorParameters* p) {
   for (int i = 1; i < (argc - 1); i++) {
       std::string tmp = argv[i];
       if (tmp.find('-') != std::string::npos) {
          if (tmp.length() > 1) {
             std::string name = tmp.substr(1, tmp.length());
             std::string value = argv[i + 1];
             p->setValue(name.c_str(), value.c_str());

          }
       }
    }
}


