// read a file into memory
#include <iostream>
#include "EffectsProcessor.h"


int main(const int argc, const char **argv) {


    EffectsProcessor p;
    if (!p.setFromConsoleArguments(argc,argv))
        std::cout << p.getLastError() << std::endl;
    else {
        if (!p.process())
            std::cout << p.getLastError() << std::endl;
    }

    return 0;
}
