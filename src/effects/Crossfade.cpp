/*
 * Crossfade.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "effects/Crossfade.h"
#include "ProcessorParameters.h"

#include <fstream>
// System includes
#include <stdio.h>

#include "frameio/BufferedYUVFrameWriter.h"
#include "frameio/BufferedYUVFrameReader.h"
#include "OpenCLAdapter.h"
#include "helpers/helpers.h"
#include "SourceProcessor.h"
#include "FrameCalculator.h"

#include "loggers/SimpleLog.h"
#include "resource.h"

Crossfade::Crossfade(const ProcessorParameters &tok) :
      Effect("crossfade", tok)

{

}
Crossfade::~Crossfade() {

}

std::string Crossfade::getProgramSource() {

   SourceProcessor processor;
#ifdef VISUALSTUDIO
   processor.addSourceFromVisualStudioResource(INTERPOLATE);
   processor.addSourceFromVisualStudioResource(CROSSFADE);
#else
   extern char _binary_interpolate_cl_start;
   extern char _binary_interpolate_cl_end;
   processor.addSourceFromExternObj(_binary_interpolate_cl_start,
       _binary_interpolate_cl_end);

   extern char _binary_crossfade_cl_start;
   extern char _binary_crossfade_cl_end;
   processor.addSourceFromExternObj(_binary_crossfade_cl_start,
         _binary_crossfade_cl_end);
#endif

   if (!processor.isSourceLoaded())
      throw std::runtime_error("Could not load Kernel source code!");

   processor.replaceParams(params);

   processor.replaceStrings("{EffectLength}", to_string<size_t>(EffectLength));
   processor.replaceStrings("{BufferSize}", to_string<size_t>(inputFile1->BufferSize));

   processor.replaceStrings("{Start}", to_string<size_t>(this->StartFrame));


 //  processor.replaceStrings("{FrameFactor}", to_string<double>(fcalc->FrameScaleFactor));
   //{Start};
   //{EffectLength};
   //{FrameStep};


   return processor.getSource();
}

void Crossfade::initGlobalWorksize(OpenCLAdapter& mgr) {
   if (params.USize != params.VSize)
      throw std::logic_error(
            "Logic error: U block size must be the same as V!");
   int size = 2;
   size_t *tmpglobalWorkSize = new size_t[size];
   tmpglobalWorkSize[0] = params.Width;
   tmpglobalWorkSize[1] = params.Height;
   mgr.setWorkSize(tmpglobalWorkSize, size);
}


