/*
 * SlideIn.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "effects/SlideIn.h"
#include "frameio/BufferedYUVFrameReader.h"

#include "ProcessorParameters.h"

#include <exception>
#include "OpenCLAdapter.h"
#include "SourceProcessor.h"
#include "helpers/helpers.h"
#include "resource.h"

SlideIn::SlideIn(const ProcessorParameters &tok) :
      Effect("slidein", tok), direction(setDirection(tok))

{

}
SlideIn::~SlideIn() {

}

void SlideIn::replaceSourceTags(SourceProcessor& processor) {
   processor.replaceParams(params);
   processor.replaceStrings("{EffectLength}", to_string<size_t>(EffectLength));
   processor.replaceStrings("{BufferSize}", to_string<size_t>(inputFile1->BufferSize));

   size_t frameSize = params.YSize;
   processor.replaceStrings("{FrameSize}", to_string<size_t>(frameSize*sizeof(unsigned char)));

   processor.replaceStrings("{Start}", to_string<size_t>(this->StartFrame));
   switch (direction) {
   case RIGHT:
   case LEFT:
      processor.replaceStrings("{ByExpr}", "Ay");
      processor.replaceStrings("{SlidePosMultX}", to_string<size_t>(params.Width));
      processor.replaceStrings("{SlidePosMultY}", "0");
      break;
   case UP:
   case DOWN:
      processor.replaceStrings("{BxExpr}", "Ax");
      processor.replaceStrings("{SlidePosMultX}", "0");
      processor.replaceStrings("{SlidePosMultY}", to_string<size_t>(params.Height));
      break;
   case RIGHTUP:
   case RIGHTDOWN:
   case LEFTUP:
   case LEFTDOWN:
      processor.replaceStrings("{SlidePosMultX}", to_string<size_t>(params.Width));
      processor.replaceStrings("{SlidePosMultY}", to_string<size_t>(params.Height));
      break;
   default:
      break;
   }
   switch (direction) {
   case RIGHT:
      processor.replaceStrings("{SlideCondition}", "Ax < thresholdX");
      processor.replaceStrings("{BxExpr}",
            to_string<size_t>(params.Width) + " - (thresholdX - Ax)");
      break;
   case LEFT:
      processor.replaceStrings("{SlideCondition}",
            std::string("(") + to_string<size_t>(params.Width) + " - Ax) < thresholdX");
      processor.replaceStrings("{BxExpr}",
            std::string("thresholdX - (") + to_string<size_t>(params.Width) + " - Ax)");
      break;
   case UP:
      processor.replaceStrings("{SlideCondition}",
            std::string("(") + to_string<size_t>(params.Height)
                  + " - Ay) < thresholdY");
      processor.replaceStrings("{ByExpr}",
            std::string("thresholdY - (") + to_string<size_t>(params.Height)
                  + " - Ay)");
      break;
   case DOWN:
      processor.replaceStrings("{SlideCondition}", "Ay < thresholdY");
      processor.replaceStrings("{ByExpr}",
            to_string<size_t>(params.Height) + " - (thresholdY - Ay)");
      break;
   case RIGHTUP:
      processor.replaceStrings("{SlideCondition}",
            std::string("((") + to_string<size_t>(params.Height)
                  + " - Ay) < thresholdY) && (Ax < thresholdX)");
      processor.replaceStrings("{BxExpr}",
            to_string<size_t>(params.Width) + " - (thresholdX - Ax)");
      processor.replaceStrings("{ByExpr}",
            std::string("thresholdY - (") + to_string<size_t>(params.Height)
                  + " - Ay)");
      break;
   case RIGHTDOWN:
      processor.replaceStrings("{SlideCondition}",
            " ( Ay < thresholdY ) && ( Ax < thresholdX ) ");
      processor.replaceStrings("{BxExpr}",
            to_string<size_t>(params.Width) + " - (thresholdX - Ax)");
      processor.replaceStrings("{ByExpr}",
            to_string<size_t>(params.Height) + " - (thresholdY - Ay)");
      break;
   case LEFTUP:
      processor.replaceStrings("{SlideCondition}",
            std::string("((") + to_string<size_t>(params.Width)
                  + " - Ax) < thresholdX) && ((" + to_string<size_t>(params.Height)
                  + " - Ay) < thresholdY)");
      processor.replaceStrings("{BxExpr}",
            std::string("thresholdX - (") + to_string<size_t>(params.Width) + " - Ax)");
      processor.replaceStrings("{ByExpr}",
            std::string("thresholdY - (") + to_string<size_t>(params.Height)
                  + " - Ay)");
      break;
   case LEFTDOWN:
      processor.replaceStrings("{SlideCondition}",
            std::string("( Ay < thresholdY ) && ((") + to_string<size_t>(params.Width)
                  + " - Ax) < thresholdX)");
      processor.replaceStrings("{BxExpr}",
            std::string("thresholdX - (") + to_string<size_t>(params.Width) + " - Ax)");
      processor.replaceStrings("{ByExpr}",
          to_string<size_t>(params.Height) + " - (thresholdY - Ay)");
      break;
   default:
      break;
   }
}

/**
 * Load source of the kernel. Replace appropriate values in curly braces tags {} with
 * values provided at runtime. This way we don't have to use 10 function parameters for the kernel.
 */
std::string SlideIn::getProgramSource() {

   if (direction == 0)
      throw std::invalid_argument("Slide direction not set");

   SourceProcessor processor;
#ifdef VISUALSTUDIO
   processor.addSourceFromVisualStudioResource(INTERPOLATE);
   processor.addSourceFromVisualStudioResource(SLIDEIN);
#else

   extern char _binary_interpolate_cl_start;
   extern char _binary_interpolate_cl_end;
   processor.addSourceFromExternObj(_binary_interpolate_cl_start,
       _binary_interpolate_cl_end);

   extern char _binary_slidein_cl_start;
   extern char _binary_slidein_cl_end;
   processor.addSourceFromExternObj(_binary_slidein_cl_start,
         _binary_slidein_cl_end);
#endif

   if (!processor.isSourceLoaded())
      throw std::runtime_error("Could not load Kernel source code!");

   replaceSourceTags(processor);

   return processor.getSource();
}

SlideDirections SlideIn::setDirection(const ProcessorParameters &tok) {
   std::string tmp = tok.getValue("d");
   if (tmp == "up")
      return UP;
   if (tmp == "down")
      return DOWN;
   if (tmp == "left")
      return LEFT;
   if (tmp == "right")
      return RIGHT;

   //corners
   if (tmp == "leftup")
      return LEFTUP;
   if (tmp == "leftdown")
      return LEFTDOWN;
   if (tmp == "rightup")
      return RIGHTUP;
   if (tmp == "rightdown")
      return RIGHTDOWN;

   throw std::invalid_argument("Invalid direction: available directions:\n"
         "down\n"
         "left\n"
         "right\n"
         "rightup\n"
         "rightdown\n"
         "leftup\n"
         "leftdown\n");
}
void SlideIn::initGlobalWorksize(OpenCLAdapter& mgr) {
   if (params.USize != params.VSize)
      throw std::logic_error(
            "Logic error: U block size must be the same as V!");
   int size = 2;
   size_t *tmpglobalWorkSize = new size_t[size];
   tmpglobalWorkSize[0] = params.Width;
   tmpglobalWorkSize[1] = params.Height;
   mgr.setWorkSize(tmpglobalWorkSize, size);
}

