
void crossfadePixel( __global unsigned char * output, size_t Y_Pos, __global unsigned char * inputA, __global unsigned char * inputA_previous, __global unsigned char * inputB, __global unsigned char * inputB_previous, float interpolateBy, size_t x, size_t y,  float progress )
{
		   unsigned char Ay = interpolate(inputA[Y_Pos],inputA_previous[Y_Pos],interpolateBy);
		   unsigned char By = interpolate(inputB[Y_Pos],inputB_previous[Y_Pos],interpolateBy);
		   
		   output[Y_Pos] = Ay - ((float)( (int)Ay - (int)By) * progress);
		   if ( ((x % 2) == 0) &&  ((y % 2) == 0) )
		   { 
			 unsigned long int offset = ((y/2)* ({Width}/2))+(x/2);
			 unsigned long int U_pos = {UBlockOffset} + offset ;
			 unsigned long int V_pos = {VBlockOffset} + offset;
			 
			 
			 unsigned char Au = interpolate(inputA[U_pos],inputA_previous[U_pos],interpolateBy);
		     unsigned char Bu = interpolate(inputB[U_pos],inputB_previous[U_pos],interpolateBy);
		     
		     unsigned char Av = interpolate(inputA[V_pos],inputA_previous[V_pos],interpolateBy);
		     unsigned char Bv = interpolate(inputB[V_pos],inputB_previous[V_pos],interpolateBy);
			 
			 output[U_pos] = Au - ((float)( (int)Au - (int)Bu) * progress);
			 output[V_pos] = Av - ((float)( (int)Av - (int)Bv) * progress);
		   }
}




__kernel void crossfade(__constant float *frameNo,
 __global unsigned char *inputA,
 __global unsigned char *inputA_previous,
  __global unsigned char *inputB,
  __global unsigned char *inputB_previous,
   __global unsigned char *output)
{

   size_t x = get_global_id(0);
   size_t y = get_global_id(1);
   size_t Y_Pos = ((y)* {Width})+(x);
   
   size_t startFrame = {Start};
   size_t effectLength = {EffectLength};
   float interpolateBy = ( *frameNo) - floor( *frameNo);
   
   if (   ( *frameNo) < startFrame) 
   {
   
		interpolateHard(output, Y_Pos, inputA, inputA_previous, interpolateBy, x, y);

    
   } else if ( ( *frameNo) > (startFrame + effectLength)) {
		interpolateHard(output, Y_Pos, inputB, inputB_previous, interpolateBy, x, y);
   } else 
   {
		   float progress = ((*frameNo) - startFrame) / (float) {EffectLength}; 
		   if (progress > 1.f ) progress = 1.f;
		  
		  crossfadePixel(output, Y_Pos, inputA, inputA_previous, inputB, inputB_previous,  interpolateBy, x, y, progress);
    	
   
   }
   
}