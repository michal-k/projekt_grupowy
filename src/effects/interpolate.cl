
unsigned char interpolate(unsigned char inputA,unsigned char inputA_previous, float factor)
{
	float delta = inputA - inputA_previous;
	return inputA_previous + (factor*delta);
}


void interpolateHard( __global unsigned char * output, size_t Y_Pos, __global unsigned char * inputA, __global unsigned char * inputA_previous, float interpolateBy, size_t x, size_t y )
{
    output[Y_Pos] = interpolate(inputA[Y_Pos],inputA_previous[Y_Pos],interpolateBy);
    if ( ((x % 2) == 0) &&  ((y % 2) == 0) )
    { 
        unsigned long int offset = ((y/2)* ({Width}/2))+(x/2);
        unsigned long int U_pos = {UBlockOffset} + offset ;
        unsigned long int V_pos = {VBlockOffset} + offset;
        output[U_pos] = interpolate(inputA[U_pos],inputA_previous[U_pos],interpolateBy);
        output[V_pos] = interpolate(inputA[V_pos],inputA_previous[V_pos],interpolateBy);
    }
}
