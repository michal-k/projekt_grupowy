
void slideinPixel( size_t Ax, size_t Ay,
  __global unsigned char * output,
    __global unsigned char * inputA,
         __global unsigned char * inputA_previous,
   __global unsigned char * inputB,
    __global unsigned char * inputB_previous,
    float interpolateBy,
    float progress
   )
{

    const bool calculateUV = ( ((Ax % 2) == 0) &&  ((Ay % 2) == 0) );

    unsigned long int YPosA = ((Ay)* {Width})+(Ax);
    unsigned long int UPosA;
    unsigned long int VPosA;
    if ( calculateUV )
    {
        unsigned long int offset = ((Ay/2)* ({Width}/2))+(Ax/2);
        UPosA = {UBlockOffset} + offset;
        VPosA = {VBlockOffset} + offset;
    }

    size_t thresholdX = progress * {SlidePosMultX};
    size_t thresholdY = progress * {SlidePosMultY};


    // Must take into account 4 directions left,rigth,down, up + 4 corners:
    // Left up, right up , right down, left down = 8 cases?
    if ({SlideCondition}) 
    {  
        size_t Bx = {BxExpr};
        size_t By = {ByExpr};


        unsigned long int YPosB = ((By)* ({Width}))+(Bx); 
        unsigned long int UPosB;
        unsigned long int VPosB;
        output[YPosA] = interpolate(inputB[YPosB],inputB_previous[YPosB],interpolateBy); 
   
        if ( calculateUV )
        {
            unsigned long int offset = ((By/2)* ({Width}/2))+(Bx/2);
            UPosB = {UBlockOffset} + offset;
            VPosB = {VBlockOffset} + offset;
            output[UPosA] = interpolate(inputB[UPosB],inputB_previous[UPosB],interpolateBy);
            output[VPosA] = interpolate(inputB[VPosB],inputB_previous[VPosB],interpolateBy); 
        }
    }else {

        output[YPosA] = interpolate(inputA[YPosA],inputA_previous[YPosA],interpolateBy); 
        if (calculateUV)
        {
            output[UPosA] = interpolate(inputA[UPosA],inputA_previous[UPosA],interpolateBy);
            output[VPosA] = interpolate(inputA[VPosA],inputA_previous[VPosA],interpolateBy);
        }
    }
  
}



__kernel void slidein(__constant float *frameNo,
                      __global unsigned char *inputA,
                      __global unsigned char *inputA_previous,
                      __global unsigned char *inputB,
                      __global unsigned char *inputB_previous,
                      __global unsigned char *output)
{ 
    size_t x = get_global_id(0);
    size_t y = get_global_id(1);
    size_t Y_Pos_for_interpolation = ((y)* {Width})+(x);
    
    float interpolateBy = ( *frameNo) - floor( *frameNo);
	size_t startFrame = {Start};
    size_t effectLength = {EffectLength};
    

  if (   ( *frameNo) < startFrame) 
   {
   
		interpolateHard(output, Y_Pos_for_interpolation, inputA, inputA_previous, interpolateBy, x, y);

    
   } else if ( ( *frameNo) > (startFrame + effectLength)) {
		interpolateHard(output, Y_Pos_for_interpolation, inputB, inputB_previous, interpolateBy, x, y);
   } else 
   {
		    float progress = ((*frameNo) - startFrame) / (float) {EffectLength}; 
            if (progress > 1.f ) progress = 1.f;
    
		   slideinPixel(x, y, output, inputA, inputA_previous, inputB, inputB_previous,  interpolateBy, progress);
    
   }

   

}    
