/*
 * InvalidEffectParameter.cpp
 *
 *  Created on: May 6, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "exceptions/invalid_effect_parameter.h"
#include <string>

const std::string invalid_effect_parameter::combineMessage(
      const std::string& paramName, const std::string& invalidValue) {
          std::string combined =std::string( std::string("Invalid value for ") + paramName )+ ": " + invalidValue;
   return combined;
}

invalid_effect_parameter::invalid_effect_parameter(const std::string& paramName,
      const std::string& invalidValue) :
      std::runtime_error(combineMessage(paramName, invalidValue)), paramName(
            paramName), invalidValue(invalidValue) {

}

invalid_effect_parameter::~invalid_effect_parameter() _GLIBCXX_USE_NOEXCEPT {
   // TODO Auto-generated destructor stub
}

