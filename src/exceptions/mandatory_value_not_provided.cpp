/*
 * mandatoryvaluenotprovided.cpp
 *
 *  Created on: May 7, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "exceptions/mandatory_value_not_provided.h"

mandatory_value_not_provided::mandatory_value_not_provided(
      const std::string& paramName) :
      runtime_error(std::string("Mandatory value not provided: ")+paramName)

{

}

mandatory_value_not_provided::~mandatory_value_not_provided() _GLIBCXX_USE_NOEXCEPT {
}

