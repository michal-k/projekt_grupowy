/*
* ProcessorParameters.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "frameio/BufferedYUVFrame.h"
#include <stdexcept>
#include <string.h>

BufferedYUVFileProcessor::BufferedYUVFileProcessor(const std::string& Filename,
                                                   const YUVFileParams &params) :
Filename(Filename), params(params), FrameSize(
    params.YSize + params.USize + params.VSize), BufferSize(FrameSize),
    buffer(0) {

        buffer = new char[BufferSize * sizeof(char)];
        if (FrameSize == 0)
            throw std::logic_error("Frame size cannot be 0! Check width / height.");

}

char * BufferedYUVFileProcessor::getBuffer() const { return buffer; }

BufferedYUVFileProcessor::~BufferedYUVFileProcessor() {
    if (buffer)
        delete[] buffer;

}
