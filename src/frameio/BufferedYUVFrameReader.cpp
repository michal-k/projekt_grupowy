/*
* ProcessorParameters.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "frameio/BufferedYUVFrameReader.h"
#include <stdexcept>
#include <fstream>
#include <string.h>



BufferedYUVFrameReader::BufferedYUVFrameReader(const std::string& Filename, const YUVFileParams &params):
BufferedYUVFileProcessor(Filename,params),
inputFile(fopen(Filename.c_str(), "rb")),
FileLength(getLength()),
FrameCount(FileLength/FrameSize),
framesRead(0)
{
    if (!this->inputFile /*|| !inputFile->is_open()*/)
        throw std::runtime_error(
        std::string("Could not open file: ")
        + std::string(Filename));
    remainingBytes = FileLength;

    previousFrameBuffer = new char[BufferSize * sizeof(char)];
}



char * BufferedYUVFrameReader::loadNextFrame()
{
    if (hasFrame()) {
        swapBuffers();
        fread(buffer, sizeof(char), BufferSize,inputFile);
        remainingBytes -= BufferSize;
        framesRead++;
        return buffer;
    }
    return 0;
}
bool BufferedYUVFrameReader::hasFrame(){
    if (remainingBytes >= BufferSize)
        return true;
    return false;
}


BufferedYUVFrameReader::~BufferedYUVFrameReader()
{
    closeFile();
    // delete inputFile;

}

size_t BufferedYUVFrameReader::getLength() {
    if (!inputFile)
        return 0;
    fseek(inputFile, 0L, SEEK_END);
    size_t length = ftell(inputFile);
    fseek(inputFile, 0L, SEEK_SET);
    return length;
}

void BufferedYUVFrameReader::closeFile()
{
    if (inputFile /*&& inputFile->is_open()*/)
    {
        fclose(inputFile);
        std::cout << "Closed: " << Filename << std::endl;
    }
}

void BufferedYUVFrameReader::swapBuffers()
{
    char *tmp = previousFrameBuffer;
    this->previousFrameBuffer = this->buffer;
    this->buffer = tmp;
}
size_t BufferedYUVFrameReader::getFramesRead() const { return framesRead; }
char * BufferedYUVFrameReader::getPreviousFrameBuffer() const { return previousFrameBuffer; }

//BufferedPartReader::BufferedPartReader(BufferedYUVFrameReader &reader)
//    :reader(reader)
//{
//    partSize = 1024;
//    remainingFromThisFrame = reader.FrameSize;
//   
//}
//
//char * BufferedPartReader::loadNext()
//{
//}
//
//BufferedPartReader::~BufferedPartReader()
//{
//}
