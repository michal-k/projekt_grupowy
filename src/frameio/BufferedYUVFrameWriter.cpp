/*
* ProcessorParameters.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "frameio/BufferedYUVFrameWriter.h"
#include <stdexcept>
#include <fstream>
#include <string.h>
#include <iostream>



BufferedYUVFrameWriter::BufferedYUVFrameWriter(const std::string& Filename,const YUVFileParams &params):
    BufferedYUVFileProcessor(Filename,params),
    outputFile(0)
{
    outputFile = fopen(Filename.c_str(), "wb");
    if (!this->outputFile /*|| !outputFile->is_open()*/)
        throw std::runtime_error(
        std::string("Could not open file: ")
        + std::string(Filename));

}



void  BufferedYUVFrameWriter::writeFrame()
{
//	std::cout << sizeof(char) << std::endl;
//    outputFile->write(buffer, BufferSize);
	fwrite(buffer, sizeof(char), BufferSize*sizeof(char), outputFile);
}

 char* BufferedYUVFrameWriter::getBuffer()
 {
     return buffer;
 }

BufferedYUVFrameWriter:: ~BufferedYUVFrameWriter()
{
   closeFile();
//   delete outputFile;
}

size_t  BufferedYUVFrameWriter::getLength() {
//	if (!outputFile)
//			return 0;
//    outputFile->seekp(0, outputFile->end);
//    int length = outputFile->tellp();
//    outputFile->seekp(0, outputFile->beg);
    return 0;
}

void BufferedYUVFrameWriter::closeFile()
{
	if (outputFile)
	{
		fclose(outputFile);
		std::cout << "Closed: " << Filename << std::endl;
	}
}

