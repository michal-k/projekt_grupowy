/*
* ProcessorParameters.cpp
*
*  Created on: Apr 2, 2014
*      Author: cr3
*/
#include "stdafx.h"
#include "frameio/FrameBufferLoader.h"
#include "frameio/BufferedYUVFrameReader.h"
#include "FrameCalculator.h"
#include <exception>


FrameBufferLoader::FrameBufferLoader(BufferedYUVFrameReader *inputFile,FrameCalculator  *fcalc, size_t offset)
: inputFile(inputFile), fcalc(fcalc), offset(offset)
{


}

char* FrameBufferLoader::getNextFrame()
{
    if ((fcalc->getCurrentInputFrame()-offset) <= 0.f)
    {
        if (inputFile->getFramesRead() == 0 )
        {   char * tmp = inputFile->loadNextFrame();
            if (tmp == 0)
                return inputFile->getBuffer();
            else return tmp;
        }
        else {
            return inputFile->getBuffer();
        }
    }

    // here is >0.0
    if ((fcalc->getCurrentInputFrame()-offset) < inputFile->getFramesRead())
    {
        return inputFile->getBuffer();
    } else 
    {
        char *result = 0;
        while ((fcalc->getCurrentInputFrame()-offset) >= inputFile->getFramesRead())
        {
            result = inputFile->loadNextFrame();
            if (result == 0)
                break;
        }
        return result;
    }

    throw std::logic_error("Nieprzewidziane! ");

}
char* FrameBufferLoader::getPreviousFrame()
{
    if (inputFile->getFramesRead() == 0 )
        return 0;
    if (inputFile->getFramesRead() == 1 )
        return inputFile->getBuffer();

    return inputFile->getPreviousFrameBuffer();
}

FrameBufferLoader::~FrameBufferLoader()
{

}