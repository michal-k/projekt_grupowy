/*
 * helpers.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: cr3
 */

#include "stdafx.h"
#include "helpers/helpers.h"
#include <string>
#include <sstream>
#include <cctype>

bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

