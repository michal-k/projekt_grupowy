/*
 * SimpleLog.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: cr3
 */
#include "stdafx.h"
#include "loggers/SimpleLog.h"
#include <iostream>
#include <sstream>

using namespace LogLevels;
using namespace std;

SimpleLog::SimpleLog() :
	enabledLevels(new bool[LogLevels::LEVEL_COUNT]),
	levelNames(new std::string*[LogLevels::LEVEL_COUNT]),
	currentStreamLogLevel(LogLevels::Info)
{
    initNames();
	resetTo(false);
}

SimpleLog::~SimpleLog() {
	delete [] enabledLevels;
	for (int i=0; i<LogLevels::LEVEL_COUNT; i++)
	{
		if (levelNames[i])
		  delete levelNames[i];
	}
	delete [] levelNames;
}

void SimpleLog::setLevel(LevelType level) {
	if (level < LogLevels::LEVEL_COUNT)
	{
		resetTo(false);
		enabledLevels[level] = true;
	}
}

void SimpleLog::enableLevel(LevelType level) {

	if (level < LogLevels::LEVEL_COUNT)
			enabledLevels[level] = true;
}

void SimpleLog::disableLevel(LevelType level) {
	if (level < LogLevels::LEVEL_COUNT)
				enabledLevels[level] = false;
}

void SimpleLog::log(LevelType level, const char *message) {
	std::cout << _getMsg(level,message) << std::endl;
}


std::string SimpleLog::_getMsg(LogLevels::LevelType &level, const char* message) {
	std::string msg("");

	if (levelNames[level])
	{
		msg += *(levelNames[level]);
		msg += ": ";
	}
	msg += message;
	return msg;

}

bool SimpleLog::isLevelEnabled(LogLevels::LevelType level) {
	if (level >= LogLevels::LEVEL_COUNT)
	  return false;
	return enabledLevels[level];
}

void SimpleLog::resetTo(bool val) {
	for (int i=0; i<LogLevels::LEVEL_COUNT; i++)
	{
		enabledLevels[i] = val;
	}
}

void SimpleLog::enableAll() {
	resetTo(true);
}

void SimpleLog::disableAll() {
	resetTo(false);
}

bool SimpleLog::_log(LogLevels::LevelType &level, const char *message) {
	if (!isLevelEnabled(level))
		return false;
	std::cout << _getMsg(level,message) << std::endl;
	return true;
}

void SimpleLog::setStreamLevel(LogLevels::LevelType level) {
	currentStreamLogLevel = level;
}

void SimpleLog::initNames() {
	for (int i=0; i<LogLevels::LEVEL_COUNT; i++)
	{
		levelNames[i] = 0;
	}

	levelNames[Info]    = new std::string("Info");
	levelNames[Warning] = new std::string("Warning");
	levelNames[Error]   = new std::string("Error");

}
